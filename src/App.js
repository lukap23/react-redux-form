import React from 'react';
// import HomePage from './components/HomePage';
import InitializeModal from './components/InitializeModal';
import NavBar from './components/NavBar';
import './App.css';

function App() {
  return(
    <div className='homepage'>
      <NavBar />
      <InitializeModal />
    </div>
  )
}

export default App;
