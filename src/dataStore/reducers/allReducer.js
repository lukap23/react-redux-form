import stepReducer from './step';
import manufacturerReducer from './manufacturer';
import servicesReducer from './services';
import discountReducer from './discount';
import costReducer from './cost';
import contactReducer from './contact';
import { combineReducers } from 'redux';

const allReducer = combineReducers({
  step: stepReducer,
  manufacturer: manufacturerReducer,
  services: servicesReducer,
  discount: discountReducer,
  cost: costReducer,
  contact: contactReducer
})

export default allReducer;