export const costReducer = (state = {gross: 0, net: 0, diff: 0}, action) => {
  switch (action.type) {
    case 'ADD_GROSS':
      return { ...state, gross: action.payload }
    case 'ADD_NET':
      return { ...state, net: action.payload }
    case 'ADD_DIFF':
      return { ...state, diff: action.payload }  
    case 'RESET_COST':
      return { gross: 0, net: 0, diff: 0 }
    default:
      return state
  }
}

export default costReducer;