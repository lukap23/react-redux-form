export const contactReducer = (state = { firstLastName: '', email: '', phone: '', note: '' }, action) => {
  switch (action.type) {
    case 'ADD_FIRSTLASTNAME':
      return { ...state, firstLastName: action.payload }
    case 'ADD_EMAIL':
      return { ...state, email: action.payload }
    case 'ADD_PHONE':
      return { ...state, phone: action.payload }
    case 'ADD_NOTE':
      return { ...state, note: action.payload }
    case 'RESET_CONTACT':
      return { firstLastName: '', email: '', phone: '', note: '' }
    default:
      return state
  }
}

export default contactReducer;