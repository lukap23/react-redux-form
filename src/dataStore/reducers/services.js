const servicesReducer = (state = [], action) => {

  switch (action.type) {
    case 'ADD_SERVICE':
      return [...state, action.payload]
    case 'REMOVE_SERVICE':
      return state.filter(({ serviceName }) => !serviceName.includes(action.payload.serviceName))
    case 'RESET_SERVICES':
      return []  
    default:
      return state
  }
}

export default servicesReducer;