const discountReducer = (state = 0, action) => {
  switch (action.type) {
    case 'ADD_DISCOUNT':
      return action.payload
    case 'RESET_DISCOUNT':
      return 0
    default:
      return state
  }
}

export default discountReducer;