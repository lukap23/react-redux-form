const manufacturerReducer = (state = '', action) => {
  switch(action.type){
    case 'SELECT_MANUFACTURER':
      return action.payload
    case 'RESET_MANUFACTURER':
      return '' 
    default:
      return state
  }
}

export default manufacturerReducer;