export const selectManufacturer = (manufacturer) => {
  return {
    type: 'SELECT_MANUFACTURER',
    payload: manufacturer
  }
}

export const resetManufacturer = () => {
  return {
    type: 'RESET_MANUFACTURER',
  }
}