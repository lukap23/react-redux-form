export const addFirstLastname = (contactObject) => {
  return {
    type: 'ADD_FIRSTLASTNAME',
    payload: contactObject
  }
}

export const addEmail = (contactObject) => {
  return {
    type: 'ADD_EMAIL',
    payload: contactObject
  }
}

export const addPhone = (contactObject) => {
  return {
    type: 'ADD_PHONE',
    payload: contactObject
  }
}

export const addNote = (contactObject) => {
  return {
    type: 'ADD_NOTE',
    payload: contactObject
  }
}

export const resetContact = () => {
  return{
    type: 'RESET_CONTACT'
  }
}