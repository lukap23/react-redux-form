export const addGross = (gross) => {
  return {
    type: 'ADD_GROSS',
    payload: gross
  }
}

export const addNet = (net) => {
  return {
    type: 'ADD_NET',
    payload: net
  }
}

export const addDiff = (diff) => {
  return {
    type: 'ADD_DIFF',
    payload: diff
  }
}

export const resetCost = () => {
  return {
    type: 'RESET_COST'
  }
}