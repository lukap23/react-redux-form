export const addService = (serviceObject) => {
  return {
    type: 'ADD_SERVICE',
    payload: serviceObject
  }
}

export const removeService = (serviceObject) => {
  return {
    type: 'REMOVE_SERVICE',
    payload: serviceObject
  }
}

export const resetServices = () => {
  return {
    type: 'RESET_SERVICES',
  }
}