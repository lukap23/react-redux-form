export const next = () => {
  return {
    type: 'NEXT_STEP',
  }
}

export const prev = () => {
  return {
    type: 'PREV_STEP',
  }
}

export const setStep = (step) => {
  return {
    type: 'SET_STEP',
    payload: step 
  }
}

export const resetStep = () => {
  return {
    type: 'RESET',
  }
}