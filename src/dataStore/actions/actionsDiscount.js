export const addDiscount = (discount) => {
  return {
    type: 'ADD_DISCOUNT',
    payload: discount
  }
}

export const resetDiscount = () => {
  return {
    type: 'RESET_DISCOUNT',
  }
}