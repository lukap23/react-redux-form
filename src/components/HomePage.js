import React from 'react';
import InitializeModal from './InitializeModal';
import NavBar from './NavBar';

export default function HomePage(){

  return(
    <div className='homepage'>
      <NavBar />
      <InitializeModal />
    </div>
  )
}