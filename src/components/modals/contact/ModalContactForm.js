import React, {useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addFirstLastname, addEmail, addPhone, addNote } from '../../../dataStore/actions/actionsContact';

export default function ModalContactForm(props) {

  const dispatch = useDispatch();

  const reduxContact = useSelector(state => state.contact)

  // setContact in handleInput can accept either event.target.value or redux.contact values
  const [local, setLocalContact] = useState(
    {
      firstLastName: reduxContact.firstLastName,
      email: reduxContact.email,
      phone: reduxContact.phone,
      note: reduxContact.note
    }
  )
  
  const handleInput = (event) => {

    // eslint-disable-next-line default-case
    switch(event.target.name){
      case 'firstLastName':
        setLocalContact(event.target.value)
        dispatch(addFirstLastname(event.target.value));
        break
      case 'email':
      if (event.target.validity.valid){
        setLocalContact(event.target.value)
        dispatch(addEmail(event.target.value))
        }
      break
      case 'phone':
        // const phoneCheck = /^([+]?[0-9]{3})?[0-9]{8,10}$/;
        if (event.target.validity.valid){
          setLocalContact(event.target.value)
          dispatch(addPhone(event.target.value));
        }
        break
      case 'note':
        setLocalContact(event.target.value)
        dispatch(addNote(event.target.value));
        break
    }
  } 
  return (
    <div>
      <div className="modal-body">
        <div className="content">
          <div className="container">
            <h5 className='mx-2'>Korak 3. Vaši kontakt podaci</h5>            
          </div>
          <div className="container">
          <div className="form-group columns flex-centered">
            <div className="contact column col-12">
              <div className="container">
                  <input className="form-input" type="text" name="firstLastName" value={local.firstLastName} autoComplete="off" placeholder="Ime i prezime*" onChange={handleInput}/>
              </div>
              <div className="container">
                  <input className="form-input" type="email" name="email" value={local.email} autoComplete="off" placeholder="Email adresa*" onChange={handleInput}/>
              </div>
            </div>
            <div className="contact column col-12">
              <div className="container">
                  <input className="form-input" type="text" name="phone" value={local.phone} autoComplete="off" pattern="^([+]?[0-9]{3})?[0-9]{8,10}$" placeholder="Broj telefona*" onChange={handleInput}/>
              </div>
              <div className="container">
                  <textarea className="form-input" name="note" value={local.note} placeholder="Napomena (opcionalno)" rows="3" onChange={handleInput} />
              </div>
            </div>            
          </div>
          </div>
        </div>
      </div>
      <div className="modal-footer mx-2">
        <div className="divider"></div>
        <div className="container">
          <button className='btn m-2' onClick={props.prevStep}>Nazad</button>
          <button className={reduxContact.firstLastName && reduxContact.email && reduxContact.phone ? 'btn btn-primary' : 'btn btn-primary disabled' } onClick={props.nextStep}>Dalje</button>
        </div>
      </div>
    </div>
  )
}