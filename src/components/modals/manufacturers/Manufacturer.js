import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectManufacturer } from '../../../dataStore/actions/actionsManufacturer';

export default function Manufacturer({name}){

  const dispatch = useDispatch();
  
  let selectedManufacturer = useSelector(state => state.manufacturer)

  const handleChange = event => {
    // Sets selected manufacturer in state
    dispatch(selectManufacturer(event.target.value))
  }
  
  return(
    <label className="form-radio form-inline">
      <input 
        type="radio" 
        name="manufacturer" 
        value={name} 
        checked={selectedManufacturer === name} 
        onChange={handleChange} />
        <i className="form-icon"></i> {name}
    </label>
  )
}