import React from 'react';
import Manufacturer from '../manufacturers/Manufacturer';
// Imports manufacturers array from data folder
import { manufacturersData } from '../../initialData/manufacturersData';
import { useSelector } from 'react-redux';


export default function ModalManufacturer(props){

  let manufacturer = useSelector(state => state.manufacturer)

  return(
    <div>
      <div className="modal-body mt-2">
        <div className="content">
          <h5>Korak 1. Odaberite proizvođača Vašeg vozila</h5>
          <div className="form-group manufacturers">
            {/* Iterates over manufacturers array and returns individual Manufacturer radio button components  */}
            {manufacturersData.map(manufName => <Manufacturer 
                                                  key={manufName} 
                                                  name={manufName} 
                                                />
                                              )
            }
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <div className="divider"></div>
        {/* If the user does not select the manufacturer, disable the button */}
        <button className={!manufacturer ? 'btn btn-primary disabled' : 'btn btn-primary'} onClick={props.nextStep}>Dalje</button>
      </div>
    </div>
  )
}