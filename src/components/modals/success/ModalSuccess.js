import React from 'react';

export default function ModalSuccess(){
  return(
    <div className='container success flex-centered'>
      <div className='success-contents'>
        <h4 className='text-center'>Vaša prijava je uspješno poslana</h4>
        <p className='text-center'>Vaša prijava je uspješno poslana i zaprimljena. Kontaktirati ćemo Vas u najkraćem mogućem roku.</p>
      </div>
    </div>
    
  )
}