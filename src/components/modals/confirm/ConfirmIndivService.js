import React from 'react';

export default function ConfirmIndivService(props) {
  return(
    <div className='overview-service'>
      <span>{props.serviceName}</span>
      <span>{props.cost.toFixed(2).replace('.', ',')} KN</span>
    </div>
  )
}