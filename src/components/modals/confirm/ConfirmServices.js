import React from 'react';
import { useSelector } from 'react-redux';
import ConfirmIndivService from './ConfirmIndivService';

export default function ConfirmServices(props){

  const services = useSelector(state => state.services)
  const discount = useSelector(state => state.discount)
  const cost = useSelector(state => state.cost)

  return(
    <div className="column col-6">
      <div className="column col-12 overview-section-header">
        <h5>ODABRANE USLUGE</h5>
        <button className='btn btn-primary btn-sm mx-2' value='2' onClick={props.handleClick}>Uredi</button>
      </div>
      <div className="column col-12">
        {/* Map over selected services from state and print */}
        {services.map(service => 
          <ConfirmIndivService serviceName={service.serviceName} cost={service.cost} key={service.serviceName} />
          )}
          <div className="overview-total">
            {discount !== 0 &&
            <span className='my-1'>POPUST ({discount}%) {`-${cost.diff.toFixed(2).replace('.', ',')} KN`}</span>}
            <span className='my-1'>UKUPNO: <b>{cost.net.toFixed(2).replace('.', ',')} KN</b></span>
          </div>
      </div>
    </div>
  )
}

