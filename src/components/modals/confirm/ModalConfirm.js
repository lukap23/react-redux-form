import React from 'react';
import ConfirmManuf from './ConfirManuf';
import ConfirmServices from './ConfirmServices';
import ConfirmContact from './ConfirmContact';
import { setStep } from '../../../dataStore/actions/actionsFormSteps';
import { useDispatch } from 'react-redux';


export default function ModalContactForm(props) {  
  
  const dispatch = useDispatch();

  // Used for handling form steps after user clicks 'Uredi' buttons 
  const handleClick = (event) => {
    dispatch(setStep(parseInt(event.target.value)))
  }

  return (
    <div>
      <div className="modal-body">
        <div className="content overview-content">
          <div className="overview-step">
            <h6>Korak 4. Pregled i potvrda Vašeg odabira</h6>
            <span>Molimo Vas da još jednom pregledate i potvrdite unesene podatke. Ukoliko želite promijeniti neki od podataka, možete pritisnuti gumb za uređivanje pored svake od kategorija. Kada ste provjerili i potvrdili ispravnost svojih podataka, pritisnite gumb pošalji na dnu, za slanje upita za servis.</span>
          </div>
          <div className="container overview-sections-wrapper my-2">
            <div className="columns my-2">
              <ConfirmManuf handleClick={handleClick}/>
              <ConfirmServices handleClick={handleClick}/>
              <ConfirmContact handleClick={handleClick}/>
            </div>
          </div>
        </div>
      </div>
      <div className="modal-footer modal-footer-overview">
        <button className='btn mx-2' onClick={props.prevStep}>Nazad</button>
        <button className='btn btn-primary' onClick={props.nextStep}>Pošalji</button>
      </div>
    </div>
  )
}