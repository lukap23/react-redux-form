import React from 'react';
import { useSelector } from 'react-redux';

export default function ConfirmManuf(props){

  const manufacturer = useSelector(state => state.manufacturer)

  return(
    <div className="column col-6">
      <div className="column col-12 overview-section-header">
        <h5>MODEL VOZILA</h5>
        <button className='btn btn-primary btn-sm mx-2' value='1' onClick={props.handleClick}>Uredi</button>
      </div>
      <div className="column col-12">
        <span>{manufacturer}</span>
      </div>
    </div>
  )
}
