import React from 'react';
import { useSelector } from 'react-redux';


export default function ConfirmContact(props){

  const contact = useSelector(state => state.contact)

  return(
    <div className="column col-12">
        <div className="divider"></div>
      <div className="column col-12 overview-section-header">
        <h5>KONTAKT PODACI</h5>
        <button className='btn btn-primary btn-sm mx-2' value='3' onClick={props.handleClick}>Uredi</button>
      </div>
      <div className="column-col-12 overview-contact-body">
        <div className="column col-6">
          <div className="column col-10 overview-contact-body-contents">
            <span>Ime i prezime</span>
            <span>{contact.firstLastName}</span>
          </div>
          <div className="column col-10 overview-contact-body-contents my-2">
            <span>Broj telefona</span>
            <span>{contact.phone}</span>
          </div>
        </div>
        <div className="column col-6">
          <div className="column col-12 overview-contact-body-contents">
            <span>Email adresa</span>
            <span>{contact.email}</span>
          </div>
          {/* Render the note if the note state is not empty */}
          {contact.note &&
            <div className="column col-12 overview-contact-body-contents">
              <div className="column col-6">
                <span>Napomena</span>
              </div>
              <div className="column col-6 note my-2">
                <span className='text-right'>{contact.note}</span>
              </div>
            </div>
          }
        </div>
      </div>
    </div>
  )
} 
