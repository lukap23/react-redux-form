import React from 'react';
import Discount from './Discount';
import { useSelector } from 'react-redux';


export default function Total(props){

  const cost = useSelector(state => state.cost);

  return(
    <div>
      <div className='discount'>
        <Discount />
      </div>
      <h4 className='my-1'><span className='text-normal'>UKUPNO:</span> {cost.net.toFixed(2).replace('.', ',')} KN</h4>
    </div>
  )
}