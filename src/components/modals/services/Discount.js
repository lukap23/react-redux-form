/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';
import { discountsData } from '../../initialData/discountsData';
import { useSelector, useDispatch } from 'react-redux';
import { addDiscount } from '../../../dataStore/actions/actionsDiscount';
import { addDiff, addNet } from '../../../dataStore/actions/actionsCost';

export default function Discount(props){

  // Used for conditional rendering of elements
  const [elements, setElements] = useState(
    {
      showDiscountInput: false,
      showError: false,
      showDiscount: false,
    }
  )

  // User code input
  const [codeInput, setCodeInput] = useState('')
  const dispatch = useDispatch();
  // Cost of services (object)
  const cost = useSelector(state => state.cost);
  // Discount (if applicable)
  const discount = useSelector(state => state.discount);
  
  // Dispatches diff and net values on button click
  const calcDiff = (disc) => {
    let diff = cost.gross * (disc / 100);
      dispatch(addDiff(diff));
    
    let net = cost.gross - diff;
    dispatch(addNet(net))  
  }

  // Sets user discount code input in state
  const handleCodeInput = event => {
    setCodeInput(event.target.value);
  }

  const showInput = () => {
    setElements(elements => ({ ...elements, showDiscountInput: true, showLink: false}))
  }

  const checkInputCode = () => {
    for (let i = 0; i < discountsData.length; i++) {
      if (codeInput === discountsData[i].code) {
        setElements(elements => ({...elements, showDiscount: true}));
        dispatch(addDiscount(discountsData[i].percentage));
        calcDiff(discountsData[i].percentage);
        break
      }
    }
    if (!elements.showDiscount){
      setElements(elements => ({...elements, showError: true}));
    }
  }

  return(
    <div>
      {/* Link */}
      {discount === 0 && !elements.showDiscountInput &&
        <a onClick={showInput}>Imam kupon</a>
      }

      {/* Input & error message */}
      {elements.showDiscountInput && !elements.showDiscount &&
        <div>
          {/* Error message*/}
          {elements.showError &&
            <p className='text-error'>Unijeli ste nepostojeći kod</p>
          }
          <input type="text" placeholder='Unesite kod kupona ovdje' size='25' onChange={handleCodeInput} />
          <button className='btn btn-error btn-apply' onClick={checkInputCode}>Primjeni</button>
        </div>
      }

      {/* Discount info */}
      {discount !== 0 &&  
        <div>
          <p className='text-success'>Hvala Vam, unijeli ste ispravan kod kupona</p>
        <h6><span className='text-normal'>OSNOVICA:</span> {cost.gross.toFixed(2).replace('.', ',')} KN</h6>
        <h6>
          <span className='text-normal'>
            {/* String interpolation is used because input event returns string, otherwise use !=for comparison */}
            POPUST {`(${discount}%)`}: </span> {cost.diff !== 0 ? `-${cost.diff.toFixed(2).replace('.', ',')}` : cost.diff} KN
        </h6>
        </div> 
      }                             
    </div>
  )
}