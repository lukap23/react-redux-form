/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import Service from './Service';
import Total from './Total';
import { useSelector } from 'react-redux';

export default function ModalServices(props) {

  const selectedServices = useSelector(state => state.services)

  return (
    <div>
      <div className="modal-body">
        <div className="content">
          <h5>Korak 2. Odaberite jednu ili više usluga za koje ste</h5>
          <div className="form-group services">
            {/* Iterates over services array and returns Service checkbox components  */}
            {props.servicesData.map(service => <Service key={service.serviceName} service={service} />)} 
          </div>   
        </div>
      </div>
      <div className="modal-footer modal-footer-services">
        <Total />
        <div>          
          <div className="divider"></div>
          <button className='btn mr-2' onClick={props.prevStep}>Nazad</button>
          {/* If the user does not select at least one service, disable the button */}
          <button className={!selectedServices.length ? 'btn btn-primary disabled' : 'btn btn-primary'} onClick={props.nextStep}>Dalje</button>
        </div>        
      </div>
    </div>
  )
}