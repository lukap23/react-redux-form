import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addService, removeService } from '../../../dataStore/actions/actionsServices';
import { addGross, addNet, addDiff } from '../../../dataStore/actions/actionsCost';
import { servicesData } from '../../initialData/servicesData';


export default function Service(props) {

  const dispatch = useDispatch();

  const discount = useSelector(state => state.discount)

  // Calculates total cost of selected services, including discounts
  const calculateTotalCost = (servicesArray) => {
    let gross = 0; 
    let diff = 0;
    let net = 0;

    gross = servicesArray.map(service => service.checked ? service.cost : 0).reduce((accumulator, currentValue) => {
      return accumulator + currentValue
    }, 0);

    dispatch(addGross(gross));
    
    diff = (gross * (discount / 100));
    dispatch(addDiff(diff));
    
    net = gross - diff;
    dispatch(addNet(net))  
  }
  
  // Controls checkbox selection and sets selected services state
  const handleChange = (service) => {
    // eslint-disable-next-line default-case
    switch(service.checked){
      // If service is not checked
      case false:
        service.checked = !service.checked;
        dispatch(addService(service));
        break
      // If service is checked
      case true:
        service.checked = !service.checked;
        dispatch(removeService(service));
        break    
    }
    calculateTotalCost(servicesData);
  }

  return (
    <label className="form-checkbox form-inline">
      <input 
        type="checkbox" 
        checked={props.service.checked} 
        onChange={() => {handleChange(props.service)}}
        /> 
      <i className="form-icon"></i> {`${props.service.serviceName} (${props.service.cost} kn)`}
    </label>
  )
}