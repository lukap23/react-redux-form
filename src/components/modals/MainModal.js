/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import ModalManufacturer from './manufacturers/ModalManufacturers';
import ModalServices from './services/ModalServices';
import ModalContactForm from './contact/ModalContactForm';
import ModalConfirm from './confirm/ModalConfirm';
import ModalSuccess from './success/ModalSuccess';
import { useSelector, useDispatch} from 'react-redux';
import { next, prev } from '../../dataStore/actions/actionsFormSteps';

export default function MainModal(props) {
  
  // Used for dispatching actions to reducers
  const dispatch = useDispatch();
  
  let step = useSelector(state => state.step)

  // let step = 4;
  // Increments form step
  const nextStep = () => {
    dispatch(next())
  }
  // Decrements form step
  const prevStep = () => {
    dispatch(prev())
  }

  // Renders modal body and footer depending on the current step
  const conditionalRender = (step) => {
    // eslint-disable-next-line default-case
    switch (step) {
      case 1:
        return (
          <ModalManufacturer 
            nextStep={nextStep} 
          />
        )
      case 2:
        return (
          <ModalServices
            servicesData={props.servicesData} 
            nextStep={nextStep} 
            prevStep={prevStep} 
          />
        )
      case 3:
        return (
          <ModalContactForm 
            nextStep={nextStep} 
            prevStep={prevStep} 
          />
        )
      case 4:
        return (
          <ModalConfirm 
            nextStep={nextStep} 
            prevStep={prevStep}/>
        )
      case 5:
        return (
          <ModalSuccess />
        )
    }
  }

  return(
    <div className="modal modal-lg active">
      {/* <a href="#close" className="modal-overlay" aria-label="Close"></a> */}
      <div className="modal-container bordered">
        <div className="modal-header text-center">
          <a  
            className="btn btn-primary float-right" 
            // Button used to close the modal window
            onClick={props.toggleModal}>
              <i className="icon icon-cross"></i>
          </a>
          <div className="modal-title"><h5>Konfigurator servisa</h5></div>
        </div>
        {/* Renders modal body and footer based on the current form step */}
        {conditionalRender(step)}
      </div>
    </div>
  )
}

