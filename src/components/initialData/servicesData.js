export const servicesData = [
  {
    serviceName: 'Zamjena ulja i filtera',
    cost: 500,
    checked: false
  },
  {
    serviceName: 'Promjena pakni',
    cost: 450,
    checked: false
  },
  {
    serviceName: 'Promjena guma',
    cost: 100,
    checked: false
  },
  {
    serviceName: 'Servis klima uređaja',
    cost: 299,
    checked: false
  },
  {
    serviceName: 'Balansiranje guma',
    cost: 50,
    checked: false
  },
  {
    serviceName: 'Zamjena ulja u kočnicama',
    cost: 229,
    checked: false
  }
]

