import React, {useState} from 'react';
import MainModal from './modals/MainModal';
import { resetStep } from '../dataStore/actions/actionsFormSteps';
import { useDispatch } from 'react-redux';
import { resetDiscount } from '../dataStore/actions/actionsDiscount';
import { resetCost } from '../dataStore/actions/actionsCost';
import { resetServices } from '../dataStore/actions/actionsServices';
import { resetManufacturer } from '../dataStore/actions/actionsManufacturer';
import { resetContact } from '../dataStore/actions/actionsContact';
import { servicesData } from './initialData/servicesData';



export default function InitializeModal() {

  const dispatch = useDispatch();

  // If set to true, modal is displayed 
  let [modal, setModal] = useState(false);

  const toggleModal = () => {
    
    // Resets form screen step to 1 
    dispatch(resetStep());
    // Resets discount to 0
    dispatch(resetDiscount());
    // Resets cost object to initial values
    dispatch(resetCost());
    // Resets services to empty array 
    dispatch(resetServices());
    // Resets manufacturer to empty string 
    dispatch(resetManufacturer());
    // Resets contact data object to initial values 
    dispatch(resetContact());
    // Resets services data
    servicesData.forEach(service => {
      if (service.checked){
        service.checked = !service.checked
      }   
  })
    
    // Toggles value to true -> shows modal
    setModal(!modal)
  }

  return(
      <div className="initialize-modal">
        <div className="columns">
          <div className="column col-12">
            <h3>Pritisnite gumb niže kako biste pokrenuli</h3>
            <div className="container flex-centered">
              <button className='btn btn-primary m-2' onClick={toggleModal}>Pokreni konfigurator</button>
            </div>
          </div>
        </div>
        {modal && <MainModal toggleModal={toggleModal} servicesData={servicesData}/>}
      </div>    
  )
}