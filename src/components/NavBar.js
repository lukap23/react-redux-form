import React from 'react';

export default function NavBar() {
  return (
    <header className="navbar">
      <section className="navbar-center">
        <div className="container">
          {/* <a href="" className='navbar-brand m-2'>Logo</a> */}
          <button className='btn btn-error'>Logo</button>
        </div>
      </section>

        <section className='navbar-section'>
          <div className="container">
            <h3 >Konfigurator servisa</h3>
            <h6>Izračunajte trošak servisa</h6>
          </div>
        </section>
    </header>
  )
}