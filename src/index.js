import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// Spectre CSS library
import 'spectre.css';
// Spectre CSS icons
import 'spectre.css/dist/spectre-icons.css'
// Redux
import {createStore} from 'redux';
import { Provider } from 'react-redux';
import allReducer from './dataStore/reducers/allReducer';

const store = createStore(
  allReducer,
  // Used for debugging state in the browser
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


ReactDOM.render(
  // Gives the App component access to the Redux store
  <Provider store={store}>
    <App />
  </Provider>, 
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
