This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## The project has the following dependencies:

- Redux (https://redux.js.org/introduction/getting-started)
- Spectre CSS (https://picturepan2.github.io/spectre/)

_______________________________________________________________________

## Available Scripts

In the project directory, to run the application, use:

### `yarn start`
or
### `npm start`

Builds the app for production to the `build` folder.<br />

### `yarn build`
or
### `npm build`






